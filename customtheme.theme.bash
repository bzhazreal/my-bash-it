# shellcheck shell=bash
# shellcheck disable=SC2034 # Expected behavior for themes.

SCM_THEME_PROMPT_PREFIX="${bold_blue?}( ${bold_red?}"
SCM_THEME_PROMPT_SUFFIX="${bold_blue?} ) "
SCM_THEME_PROMPT_DIRTY=" ${red?}✗"
SCM_THEME_PROMPT_CLEAN=" ${bold_green?}✓"

VIRTUALENV_THEME_PROMPT_PREFIX="${bold_orange?}("
VIRTUALENV_THEME_PROMPT_SUFFIX=")${reset_color?}"

function prompt_command() {
	local scm_prompt_info

	if [ "${USER:-${LOGNAME?}}" = root ]; then
		cursor_color="${bold_red?}"
		user_color="${green?}"
	else
		cursor_color="${bold_green?}"
		user_color="${white?}"
	fi
	scm_prompt_info="$(scm_prompt_info)"
	PS1="$(_toolbox_prompt)$(virtualenv_prompt)${bold_cyan?}\w ${reset_color?}${scm_prompt_info}${cursor_color}→ ${normal?}"
}

safe_append_prompt_command prompt_command
