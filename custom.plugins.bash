#!/usr/bin/env bash

#-------------------------------------------------------------------------------
# Gnome shell commands.
#-------------------------------------------------------------------------------

# Switch theme for gnome shell.
function switchtheme()
{
    case $1 in
        'light')
        GSHELL_COLOR_SCHEME='default'
		GSHELL_THEME='Adwaita'
        ;;

        'dark')
        GSHELL_COLOR_SCHEME='prefer-dark'
		GSHELL_THEME='Adwaita-dark'
        ;;

        *)
        echo "Scheme not found, only dark or light are available."
        ;;
    esac
    
    gsettings set org.gnome.desktop.interface gtk-theme "${GSHELL_THEME}"
    gsettings set org.gnome.desktop.interface color-scheme "${GSHELL_COLOR_SCHEME}"
}

#-------------------------------------------------------------------------------
# Container as commands.
#-------------------------------------------------------------------------------


function composer()
{
    COMPOSER_IMAGE_NAME="docker.io/library/composer"
    podman run --rm -it --privileged -v ${PWD}:/app ${COMPOSER_IMAGE_NAME} composer ${@}
}
