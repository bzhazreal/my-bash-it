# My bash it

## About

This repo contains my personnal configuration of [bash it framework](https://github.com/Bash-it/bash-it).

## How to test

```sh
# Build image.
export IMAGE_NAME="bashit-test"
docker build -t ${IMAGE_NAME} ./
# or
make build

# Test inside the container
docker run --rm -it ${IMAGE_NAME} bash
# or
make tty

# Delete the container
docker rmi ${IMAGE_NAME}
# or
make clean
```

## Installation

```sh
bash install.sh
```
