#-------------------------------------------------------------------------------
# My bash-it Makefile
#-------------------------------------------------------------------------------

IMAGE_NAME := bashit-test
CONTAINER_ENGINE := podman

##
# Container commands.
##
.PHONY: build
build:
	${CONTAINER_ENGINE} build -t ${IMAGE_NAME} ./

.PHONY: run
run:
	${CONTAINER_ENGINE} run --rm -it ${IMAGE_NAME} bash

.PHONY: clean
clean:
	${CONTAINER_ENGINE} rmi ${IMAGE_NAME}
