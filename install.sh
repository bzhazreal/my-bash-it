#!/usr/bin/env bash

CURRENT_USER=$(whoami)
MY_BASH_IT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
BASHIT_REPO="https://github.com/Bash-it/bash-it.git"
BASHIT_DIRECTORY="/home/$CURRENT_USER/.local/share/bashit"

if command -v git &> /dev/null; then
    git clone ${BASHIT_REPO} ${BASHIT_DIRECTORY}
else
    echo "Failed to clone bash-it, git is missing from the system"
fi

if [ -f "$BASHIT_DIRECTORY/install.sh" ]; then
    bash "$BASHIT_DIRECTORY/install.sh" --silent
fi

sed -i "s/export BASH_IT_THEME='bobby'/export BASH_IT_THEME='customtheme'/g" /home/${CURRENT_USER}/.bashrc
echo 'LOGNAME=""' >> /home/${CURRENT_USER}/.bashrc

# Copy aliases file.
mkdir -p ${BASHIT_DIRECTORY}/aliases/
cp "${MY_BASH_IT_DIR}"/custom.aliases.bash ${BASHIT_DIRECTORY}/aliases/
# Copy custom theme file.
mkdir -p ${BASHIT_DIRECTORY}/custom/themes/customtheme/
cp "${MY_BASH_IT_DIR}"/customtheme.theme.bash ${BASHIT_DIRECTORY}/custom/themes/customtheme/customtheme.theme.bash
# Copy custom plugin file.
cp "${MY_BASH_IT_DIR}"/custom.plugins.bash  ${BASHIT_DIRECTORY}/plugins/custom.plugins.bash
# Copy custom lib file.
cp "${MY_BASH_IT_DIR}"/custom.bash ${BASHIT_DIRECTORY}/lib/custom.bash

if [ "$SHELL" == "/bin/bash" ];then
    echo "Happy bash-it"
else
    echo "Your current shell is $SHELL"
    echo "usermod --shell /bin/bash $CURRENT_USER && bash"
fi
