#!/usr/bin/env bash

TOOLBOX_THEME_PROMPT_PREFIX=${bold_purple?}
TOOLBOX_THEME_PROMPT_SUFFIX=${reset_color?}
TOOLBOX_THEME_SYMBOL="⬢"

# Check if current shell run inside a toolbox.
function _is_in_toolbox()
{
	if [ -f /run/.containerenv ] && [ -f /run/.toolboxenv ] ;then
		return 0
	else
		return 1
	fi
}

# Prompt diamond if shell current run inside a toolbox.
function _toolbox_prompt()
{
	if $(_is_in_toolbox);then
		echo -ne "${TOOLBOX_THEME_PROMPT_PREFIX}${TOOLBOX_THEME_SYMBOL}${TOOLBOX_THEME_PROMPT_SUFFIX} "
	fi
}
